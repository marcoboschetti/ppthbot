package main

import (
	"fmt"
	"image"
	"strings"

	"github.com/oliamb/cutter"
	"gocv.io/x/gocv"
)

var (
	cards_position = []image.Point{
		{195, 578}, //Hand 1
		{214, 578}, //Hand 2
		{225, 314}, //Floop 1
		{245, 314}, //Floop 2
		{264, 314}, //Floop 3
		{284, 314}, //Turn
		{305, 314}, //River
	}

	suits = map[string]gocv.Mat{
		"c": getImageMatFromFilePath("./templates/suits/c.png"),
		"d": getImageMatFromFilePath("./templates/suits/d.png"),
		"h": getImageMatFromFilePath("./templates/suits/h.png"),
		"s": getImageMatFromFilePath("./templates/suits/s.png"),
	}

	values = map[string]gocv.Mat{
		"2_b": getImageMatFromFilePath("./templates/values/2_b.png"),
		"3_b": getImageMatFromFilePath("./templates/values/3_b.png"),
		"5_b": getImageMatFromFilePath("./templates/values/5_b.png"),
		"6_b": getImageMatFromFilePath("./templates/values/6_b.png"),
		"7_b": getImageMatFromFilePath("./templates/values/7_b.png"),
		"8_b": getImageMatFromFilePath("./templates/values/8_b.png"),
		"9_b": getImageMatFromFilePath("./templates/values/9_b.png"),
		"T_b": getImageMatFromFilePath("./templates/values/10_b.png"),
		"J_b": getImageMatFromFilePath("./templates/values/J_b.png"),
		"Q_b": getImageMatFromFilePath("./templates/values/Q_b.png"),
		"K_b": getImageMatFromFilePath("./templates/values/K_b.png"),
		"A_b": getImageMatFromFilePath("./templates/values/A_b.png"),
		"2_r": getImageMatFromFilePath("./templates/values/2_r.png"),
		"3_r": getImageMatFromFilePath("./templates/values/3_r.png"),
		"5_r": getImageMatFromFilePath("./templates/values/5_r.png"),
		"6_r": getImageMatFromFilePath("./templates/values/6_r.png"),
		"7_r": getImageMatFromFilePath("./templates/values/7_r.png"),
		"8_r": getImageMatFromFilePath("./templates/values/8_r.png"),
		"9_r": getImageMatFromFilePath("./templates/values/9_r.png"),
		"T_r": getImageMatFromFilePath("./templates/values/10_r.png"),
		"J_r": getImageMatFromFilePath("./templates/values/J_r.png"),
		"Q_r": getImageMatFromFilePath("./templates/values/Q_r.png"),
		"K_r": getImageMatFromFilePath("./templates/values/K_r.png"),
		"A_r": getImageMatFromFilePath("./templates/values/A_r.png"),
	}
)

const (
	w = 22
	h = 34
)

func readCards(img *image.Image) []string {

	cards := []string{}
	// ran := rand.Intn(100000)
	for _, p := range cards_position {
		r := image.Rect(p.X, p.Y, p.X+w, p.Y+h)
		cardImg := cropImage(img, r)
		cardImgMat, err := ToRGB8(cardImg)
		if err != nil {
			panic(err)
		}
		borders := toBorders(cardImgMat)

		// Debug save
		// fileName := fmt.Sprintf("card_%d_%d.png", ran, idx)
		// gocv.IMWrite(fileName, borders)
		suit, suitConf := getSuit(borders)
		value, valueConf := getValue(borders)
		// fmt.Printf("Card %d: %.2f !! %.2f | %.2f \n", idx, suitConf+valueConf, suitConf, valueConf)

		if suitConf+valueConf > 1.87 {
			val := strings.Split(*value, "_")[0]
			cards = append(cards, fmt.Sprintf("%s%s", val, *suit))
		} else {
			cards = append(cards, "")
		}
	}

	return cards
}

func getSuit(imgMat gocv.Mat) (*string, float32) {
	return findBestMatch(imgMat, suits)
}

func getValue(imgMat gocv.Mat) (*string, float32) {
	return findBestMatch(imgMat, values)
}

func findBestMatch(imgMat gocv.Mat, m map[string]gocv.Mat) (*string, float32) {
	res := gocv.NewMat()
	mask := gocv.NewMat()

	lowVal := float32(400)
	suit := ""
	for s, template := range m {
		gocv.MatchTemplate(imgMat, template, &res, gocv.TmSqdiffNormed, mask)
		minVal, _, _, _ := gocv.MinMaxLoc(res)
		if minVal < lowVal {
			lowVal = minVal
			suit = s
		}
	}

	confidence := 1 - lowVal
	return &suit, confidence
}

func cropImage(img *image.Image, r image.Rectangle) image.Image {
	croppedImg, err := cutter.Crop(*img, cutter.Config{
		Width:  r.Max.X - r.Min.X,
		Height: r.Max.Y - r.Min.Y,
		Anchor: r.Min,
		Mode:   cutter.TopLeft,
	})

	if err != nil {
		fmt.Println(err.Error())
	}
	return croppedImg
}

func getImageMatFromFilePath(filePath string) gocv.Mat {
	templateMat := gocv.IMRead(filePath, gocv.IMReadAnyColor)
	return toBorders(templateMat)
}

func toBorders(mat gocv.Mat) gocv.Mat {
	// borders := gocv.NewMat()
	// gocv.Canny(mat, &borders, 100, 400)
	// return borders

	// gocv.CvtColor(mat, &mat, gocv.ColorRGBAToGray)
	// gocv.Threshold(mat, &mat, 120, 255, gocv.ThresholdBinary)

	return mat
}

func ToRGB8(img image.Image) (gocv.Mat, error) {
	bounds := img.Bounds()
	x := bounds.Dx()
	y := bounds.Dy()
	bytes := make([]byte, 0, x*y*3)

	//don't get surprised of reversed order everywhere below
	for j := bounds.Min.Y; j < bounds.Max.Y; j++ {
		for i := bounds.Min.X; i < bounds.Max.X; i++ {
			r, g, b, _ := img.At(i, j).RGBA()
			bytes = append(bytes, byte(b>>8), byte(g>>8), byte(r>>8))
		}
	}
	return gocv.NewMatFromBytes(y, x, gocv.MatTypeCV8UC3, bytes)
}
