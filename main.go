package main

import (
	"fmt"
	"image"
	"image/png"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/kbinani/screenshot"
)

var suitsCodes = map[string]string{
	"c": "♣",
	"d": "♦",
	"h": "♥",
	"s": "♠",
}

var lastState = ""

func main() {
	rand.Seed(time.Now().Unix())

	opponentsCount := 8
	var err error
	if len(os.Args) > 1 {
		opponentsCount, err = strconv.Atoi(os.Args[1])
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println("Defaulting to", opponentsCount, "opponents")
	}

	for {

		time.Sleep(1 * time.Second)
		img := getImage()
		cards := readCards(&img)
		curState := strings.Join(cards, "!")
		if curState == lastState {
			fmt.Print(".")
			continue
		}
		CallClear()
		lastState = curState

		// Calculate win chances
		winChance := calculateChances(cards, opponentsCount)

		// Print parsed hand
		fmt.Printf("\n--------\nNew scan (%d opponents):\n-------\n", opponentsCount)
		fmt.Printf("|%s |%s |\n", codes(cards[0]), codes(cards[1]))
		fmt.Printf("|%s |%s |%s |%s |%s |\n", codes(cards[2]), codes(cards[3]), codes(cards[4]), codes(cards[5]), codes(cards[6]))
		fmt.Println("--------")

		// Call predictor
		if len(cards[0]) == 0 || len(cards[1]) == 0 {
			fmt.Println("No hand yet")
			continue
		}

		fmt.Printf("Win chances: %.2f%% %s\n", winChance, getEmoji(winChance, opponentsCount))
		fmt.Println("--------")
		fmt.Println()
	}
}

func codes(s string) string {
	for c, a := range suitsCodes {
		s = strings.Replace(s, c, a, -1)
	}
	return s
}

func getTestImage() *image.Image {
	f, err := os.Open("test_screen.png")
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	image, _, err := image.Decode(f)
	return &image
}

func getImage() image.Image {
	bounds := screenshot.GetDisplayBounds(0)

	img, err := screenshot.CaptureRect(bounds)
	if err != nil {
		panic(err)
	}
	fileName := "last_screen.png"
	file, _ := os.Create(fileName)
	defer file.Close()
	png.Encode(file, img)

	return img
}

var clear map[string]func() //create a map for storing clear funcs

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func CallClear() {
	value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok {                          //if we defined a clear func for that platform:
		value() //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}

func getEmoji(chance float64, opponents int) string {
	std := 100.0 / float64(opponents+1)
	if chance < std {
		return "🔴"
	} else if chance < 1.5*std {
		return "🟡"
	} else if chance < 2*std {
		return "🔵"
	}
	return "🟢"
}
