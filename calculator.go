package main

import (
	"fmt"
	"os/exec"
	"strconv"
	"strings"
)

var opponentTemplate = "...."

func calculateChances(cards []string, opponentsCount int) float64 {

	// Build command
	myHand := fmt.Sprintf("%s%s", cards[0], cards[1])
	board := ""
	if len(cards[2]) > 0 {
		board = "--board "
		for idx, c := range cards {
			if idx > 1 && len(c) > 0 {
				board += c
			}
		}
	}

	args := []string{"-n", myHand, board}
	for i := 0; i < opponentsCount; i++ {
		args = append(args, opponentTemplate)
	}

	cmd := exec.Command("poker-odds", args...)
	out, err := cmd.Output()
	if err != nil {
		fmt.Println("ERR in predictor:", err.Error())
	} else {
		// fmt.Println(string(out))
		winChancesLine := strings.Split(string(out), "\n")[2]
		segments := strings.Split(winChancesLine, " ")

		if len(segments) < 10 {
			fmt.Println("Strange exec output:", len(segments), "-", winChancesLine)
			return 0
		}

		valWin, err := strconv.ParseFloat(strings.TrimSuffix(segments[6], "%"), 64)
		if err != nil {
			panic(err)
		}
		valTie, err := strconv.ParseFloat(strings.TrimSuffix(segments[10], "%"), 64)
		if err != nil {
			panic(err)
		}
		return valWin + valTie
	}

	return 0
}
